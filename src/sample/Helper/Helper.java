package sample.Helper;

import javax.swing.*;

public class Helper {

    public static void showMessage(String infoMessage, String title){

        JOptionPane.showMessageDialog(null, infoMessage,  title, JOptionPane.INFORMATION_MESSAGE);

    }

}
