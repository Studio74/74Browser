package sample.Logic;



import sample.Helper.Helper;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Verschluesselung {

    public static final String TAG = "YourAppName";

    private static String TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static String ALGORITHM = "AES";
    private static String DIGEST = "MD5";

    private static Cipher _cipher;
    private static SecretKey _password;
    private static IvParameterSpec _IVParamSpec;

    //16-byte private key
    private static byte[] IV = "ThisIsUrPassword".getBytes();


    public Verschluesselung(){

        try {

            String password = "102Boyz";

            //Encode digest
            MessageDigest digest;
            digest = MessageDigest.getInstance(DIGEST);
            _password = new SecretKeySpec(digest.digest(password.getBytes()), ALGORITHM);

            //Initialize objects
            _cipher = Cipher.getInstance(TRANSFORMATION);
            _IVParamSpec = new IvParameterSpec("ThisIsUrPassword".getBytes());

        } catch (NoSuchAlgorithmException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
        } catch (NoSuchPaddingException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
        }

    }





    public String encrypt(byte[] text) {

        byte[] encryptedData;

        try {

            _cipher.init(Cipher.ENCRYPT_MODE, _password, _IVParamSpec);
            encryptedData = _cipher.doFinal(text);

        } catch (InvalidKeyException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (InvalidAlgorithmParameterException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (IllegalBlockSizeException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (BadPaddingException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        }


        return Base64.getEncoder().encodeToString(encryptedData);


    }


    public static byte[] fromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


    public String decrypt(String text) {

        String json = null;

        try {


            String password = "102Boyz";


            //Encode digest
            MessageDigest digest;
            digest = MessageDigest.getInstance(DIGEST);
            _password = new SecretKeySpec(digest.digest(password.getBytes()), ALGORITHM);

            //Initialize objects
            _cipher = Cipher.getInstance(TRANSFORMATION);
            _IVParamSpec = new IvParameterSpec(IV);



            _cipher.init(Cipher.DECRYPT_MODE, _password, _IVParamSpec);

            //byte[] decodedValue = Base64.decode(text.getBytes(), Base64.DEFAULT);
            byte[] decodedValue = Base64.getDecoder().decode(text.getBytes());

            byte[] decryptedVal = _cipher.doFinal(decodedValue);
            String tester = new String(decryptedVal);
            String controlle = tester;
            json = tester;
            return controlle;


        } catch (InvalidKeyException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (InvalidAlgorithmParameterException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (IllegalBlockSizeException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (BadPaddingException e) {
            Helper.showMessage(e.toString(), "Fehler bei der Verschlüsselung");
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return json;
    }


}
