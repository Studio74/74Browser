package sample;

import com.sun.corba.se.impl.orbutil.ObjectWriter;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.codehaus.jackson.map.ObjectMapper;
import sample.Helper.Helper;
import sample.Logic.Verschluesselung;


import java.io.*;

public class Main extends Application {


    Button createBtn, importBtn;
    RadioButton landscapeRb, portraitRb, variableRb;
    CheckBox javaScriptCb, pinchToZoomCb, zoomButtonCb, navigationBarCb;
    TextField urlTf, skalierungTf;


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 400));
        primaryStage.show();

        getControlls(primaryStage);

        setUpControlls();

    }


    public static void main(String[] args) {
        launch(args);
    }





    private void getControlls(Stage primaryStage){

        createBtn = (Button) primaryStage.getScene().lookup("#erzeugenBtn");
        importBtn = (Button) primaryStage.getScene().lookup("#importBtn");
        landscapeRb = (RadioButton) primaryStage.getScene().lookup("#landscapeRb");
        portraitRb = (RadioButton) primaryStage.getScene().lookup("#portraitRb");
        variableRb = (RadioButton) primaryStage.getScene().lookup("#variableRb");
        pinchToZoomCb = (CheckBox) primaryStage.getScene().lookup("#pinchToZoomCb");
        javaScriptCb = (CheckBox) primaryStage.getScene().lookup("#javaScriptCb");
        zoomButtonCb = (CheckBox) primaryStage.getScene().lookup("#zoomButtonCb");
        navigationBarCb = (CheckBox) primaryStage.getScene().lookup("#navigationLeisteCb");
        urlTf = (TextField) primaryStage.getScene().lookup("#urlTf");
        skalierungTf = (TextField) primaryStage.getScene().lookup("#scaleTb");

    }

    private void setUpControlls(){

        ToggleGroup toggleGroup = new ToggleGroup();

        landscapeRb.setToggleGroup(toggleGroup);
        portraitRb.setToggleGroup(toggleGroup);
        variableRb.setToggleGroup(toggleGroup);

        pinchToZoomCb.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

                zoomButtonCb.setVisible(newValue);

            }
        });

        createBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                RadioButton selectedToggle = (RadioButton) toggleGroup.getSelectedToggle();

                Configurations configurations = new Configurations();

                configurations.setJavaScript(javaScriptCb.isSelected());
                configurations.setPinchToZoom(pinchToZoomCb.isSelected());
                configurations.setNavigationBar(navigationBarCb.isSelected());
                configurations.setZoomButton(zoomButtonCb.isSelected());
                configurations.setOrientation(selectedToggle.getText());
                configurations.setUrl(urlTf.getText());
                configurations.setSkalierung(skalierungTf.getText());

                ObjectMapper objectMapper = new ObjectMapper();

                try {
                    Verschluesselung verschluesselung = new Verschluesselung();
                   String test = verschluesselung.encrypt(objectMapper.writeValueAsString(configurations).getBytes());

                   String dat = verschluesselung.decrypt(test);

                    try {
                        String userHomeFolder = System.getProperty("user.home");
                        File file = new File(userHomeFolder + "/config.pike");
                        FileWriter fileWriter = new FileWriter(file);
                        fileWriter.write(dat);
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

    }

}
