package sample;

public class Configurations {

    private boolean javaScript = true;
    private boolean navigationBar = true;
    private boolean pinchToZoom = true;
    private boolean zoomButton = true;
    private String orientation;
    private String url;
    private String skalierung = "100";


    public boolean isJavaScript() {
        return javaScript;
    }
    public void setJavaScript(boolean javaScript) {
        this.javaScript = javaScript;
    }

    public boolean isNavigationBar() {
        return navigationBar;
    }
    public void setNavigationBar(boolean navigationBar) {
        this.navigationBar = navigationBar;
    }

    public boolean isPinchToZoom() {
        return pinchToZoom;
    }
    public void setPinchToZoom(boolean pinchToZoom) {
        this.pinchToZoom = pinchToZoom;
    }

    public boolean isZoomButton() {
        return zoomButton;
    }
    public void setZoomButton(boolean zoomButton) {
        this.zoomButton = zoomButton;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSkalierung() {
        return skalierung;
    }
    public void setSkalierung(String skalierung) {
        this.skalierung = skalierung;
    }

    public String getOrientation() {
        return orientation;
    }
    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }


}
